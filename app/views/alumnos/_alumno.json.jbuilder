json.extract! alumno, :id, :nombre, :numero_de_control, :semestre, :carrera, :created_at, :updated_at
json.url alumno_url(alumno, format: :json)
