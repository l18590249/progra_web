json.extract! carrera, :id, :carrera, :created_at, :updated_at
json.url carrera_url(carrera, format: :json)
