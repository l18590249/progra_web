json.extract! docente, :id, :nombre, :created_at, :updated_at
json.url docente_url(docente, format: :json)
