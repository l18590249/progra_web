json.extract! actividad, :id, :nombre, :deportivo, :cultural, :docente, :horario, :created_at, :updated_at
json.url actividad_url(actividad, format: :json)
