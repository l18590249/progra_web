Rails.application.routes.draw do
  get 'index/index'
  devise_for :users
  resources :actividades
  resources :alumnos
  resources :docentes
  resources :tipos
  resources :carreras
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  root "index#index"
end
