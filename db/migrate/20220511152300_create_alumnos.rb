class CreateAlumnos < ActiveRecord::Migration[7.0]
  def change
    create_table :alumnos do |t|
      t.string :nombre
      t.integer :numero_de_control
      t.integer :semestre
      t.string :carrera

      t.timestamps
    end
  end
end
