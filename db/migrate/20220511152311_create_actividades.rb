class CreateActividades < ActiveRecord::Migration[7.0]
  def change
    create_table :actividades do |t|
      t.string :nombre
      t.boolean :deportivo
      t.boolean :cultural
      t.string :docente
      t.datetime :horario

      t.timestamps
    end
  end
end
