class CreateTipos < ActiveRecord::Migration[7.0]
  def change
    create_table :tipos do |t|
      t.boolean :deportivo
      t.boolean :cultural

      t.timestamps
    end
  end
end
