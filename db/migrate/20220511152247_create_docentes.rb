class CreateDocentes < ActiveRecord::Migration[7.0]
  def change
    create_table :docentes do |t|
      t.string :nombre

      t.timestamps
    end
  end
end
